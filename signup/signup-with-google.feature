@REL-790 @feature @google-signup-login
Feature: Sign up & Login with Google
As a user, I want to be able to sign up to AppTweak with my Google account so that I don�t have to enter my details manually. 

Background: Apptweak Create an Account page 
  Given I have no AppTweak account yet 
  
  @simple
  Scenario: AppTweak form is automatically filled with Google account data
    When I click on the "sign up with Google" button 
      And I sign into my google account 
    Then my information should automatically be filled in the AppTweak signup form 
    
  Scenario: User can login with Google account
    Given I have previously signed up to AppTweak with Google 
      And I am about to login into the AppTweak tool
    When I click on the "login with Google" button 
      And I enter my Google account credentials correctly 
    Then I am automatically logged into my AppTweak account 
      And I am redirected to my AppTweak ASO Dashbaord
    
    