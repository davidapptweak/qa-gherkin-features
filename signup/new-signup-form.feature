@REL-964 @feature @signup
Feature: New Signup Form

  Background: I am on the Create Account page
    Given I have no AppTweak account yet


  Scenario: Focused field is highlighted
    """
    As a user, I want the field in which I am typing information to be highlighted
    so that it is clear in which field of the form I'm on.
    """
    Given I open "Create an account" page
    When I start typing text in any field of the signup form
    Then the field border should be highlighted


  @simple @focus
  Scenario Outline: Credentials validation - valid
    """
    As a user, I want to know if my password is valid or not as soon as I have finished entering it (focus out)
    so that I immediately know if I can keep move on in the signup process or if I need to find a new password.
    """
    Given I open "Create an account" page
    When I type <value> in the <field>
      And I click outside the field
    Then the <field> should be highlighted in "green" with a green check

    Examples:
      | field      | value                 |
      | "Email"    | "user@test.e2e"       |
      | "Email"    | "user+alias@test.e2e" |
      | "Password" | "Pa$w0rd!"            |
      | "Password" | "Pa$w0rd!123456789"   |


  @simple @focus
  Scenario Outline: Credentials validation - invalid
    """
    As a user, I want to know if my password is valid or not as soon as I have finished entering it (focus out) 
    so that I immediately know if I can keep move on in the signup process or if I need to find a new password.
    """
    Given I open "Create an account" page
    When I type <value> in the <field>
      And I click outside the field
    Then the <field> should be highlighted in "red" with a red check
      And the list of validation criteria should be shown to me

    Examples:
      | field      | value        |
      | "Email"    | "user"       |
      | "Email"    | "user@test." |
      | "Password" | " "          |
      | "Password" | "P"          |
      | "Password" | "12345"      |


  @focus
  Scenario: VAT appears when user starts typing company name
    """
    As a marketer, I want the "VAT" field to appear as the user starts typing in the "Company name" field 
    so that my attention is drawn to that field.
    """
    Given I have entered my credentials as email "user@test.e2e" with password "Pa$$w0rd!"
      And I have subscribed to any plan
      And I am on the "Subscription Wizard" in "Step 2"    
    When I type "E2E test" into the "Company name"
    Then the "VAT" field should appear just below the Company name field


  @focus
  Scenario: Country is preselected based on user's geolocalisation
    """
    As a user, I want the country field to be filled automatically based on my geolocalisation 
    so that I don't have to fill it myself.
    """
    Given I have entered my credentials as email "user@test.e2e" with password "Pa$$w0rd!"
      And I have subscribed to any plan
      And I am on the "Subscription Wizard" in "Step 3"
    When I come upon the "Country" field
    Then the "Country" field should be pre-filled with the country based on my geolocalisation


  @focus
  Scenario Outline: the "state" field only appears for required countries
    """
    As a user, I don't want to see a "State" field if that field isn't appropriate for my country 
    so that I don't feel like I'm filling up a useless field
    """
    Given I have entered my credentials as email "user@test.e2e" with password "Pa$$w0rd!"
      And I am on the "Subscription Wizard" in "Step 3"
    When I enter <country-with-states> in the "Country" field
    Then the field "State" should appear

    Examples:
      | country-with-states      |
      | Canada                   |
      | United States of America |


  @focus
  Scenario Outline: Credit card type detection
    """
    As a user, I want AppTweak to show me it has detected the type of credit card I am using so that I am reassured this is a reliable website
    """
    Given I have entered my credentials as email "user@test.e2e" with password "Pa$$w0rd!"
      And I have subscribed to any plan
      And I am on the "Subscription Wizard" in "Step 3"
    When I start typing <credit-card-number> in the "Card Number input"
    Then I should see a little icon showing <credit-card-type> image
    
    Examples:
      | credit-card-number    | credit-card-type |
      | "5324348565339679"    | "mastercard"     |
      | "5293575065245157"    | "mastercard"     |
      | "4556610644018232"    | "visa"           |
      | "4508177327518788"    | "visa"           |
      | "4460857267764712304" | "visa"           |
      | "375263673968682"     | "amex"           |
      | "348712902123661"     | "amex"           |
      | "6762356092492793"    | "other"          |
      | "1234567890123456789" | "other"          |
