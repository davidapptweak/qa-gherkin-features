@feature @REL-472 @featured-apps

Feature: Featured Family Apps on Google

  Background: I am in Featured Apps page
    Given I am on "Featured App" page
      And I have clicked on the tab "family"
      And I have selected Google Play in the filters


  Scenario Outline: Most featured family apps in a category worldwide
    """
    As a user, I want to see which family apps were featured the most often in a given country and category on a given date range
    so that I can know which apps are popular on Google Play Store.
    """
    When I click on <category>
      And I don't select any other filters
    Then I should see the most featured family apps in <category> in the US, UK, France & Germany

    Examples:
      | category      |
      | Creativity    |
      | Education     |
      | Prentend Play |


  Scenario Outline: Featured Games in a category, country and language
      """
      As a user, I want to see which family apps were featured in a given country, language and category on a given date
      so that I can check if any app I know was featured.
      """

    When I click on <category>
      And I select a <country>
      And I select a <language>
    Then I should see the family apps that were featured in each group in <category> in <country> in <language>

    Examples:
      | category      | country | language  |
      | Creativity    | Norway  | norwegian |
      | Education     | Italy   | italian   |
      | Prentend Play | India   | english   |


  Scenario Outline: Worldwide featurings of a family app with no filters
      """
      As a user, I want to be able to see whether a particular family app was featured over a selected date range
      so that I can monitor where and when my app (or a competitor) was featured.
      """

    When I search for <keyword>
      And I select <family-app>
    Then I should see the list of countries and the title of the group where <family-app> was Featured

    Examples:
      | keyword                 | family-app                          |
      | disney magic kingdom    | com.gameloft.android.ANMP.GloftDYHM |
      | pepi wonder             | com.PepiPlay.KingsCastle            |
      | baby run the babysitter | com.kauf.babyrunthebabysitterescape |
      | scratchjr               | org.scratchjr.android               |
