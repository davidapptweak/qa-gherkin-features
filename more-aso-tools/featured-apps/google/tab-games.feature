@REL-472 @Featured-Apps

Feature: Featured Games on Google
    Background: I am in Featured Apps page
        Given I have selected Google Play in the filters
          And I am on the tab "Games"
    
    Scenario Outline: Most Featured Games in a category worldwide
    
    """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
    As a user, I want to see which games were featured the most often in a given country and category on a given date range
    so that I can know which games are popular on Google Play Store.
    """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
        When I click on <category>
        And I haven't selected any other filters
        Then I should see the most featured games in <category> in the US, UK, France & Germany

        Examples:
        | category       |
        | Action         |
        | Adventure      |
        | Board          |
        | Card           |
        | Casino         |

    Scenario Outline: Featured Games in a category, country and language
        
    """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
    As a user, I want to see which games were featured in a given country, language and category on a given date 
    so that I can check if any game I know was featured.
    """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

        When I click on <category>
        And I have selected a <country>
        And I have selected a <language>
        Then I should see the games that were featured in each group in <category> in <country> in <language>

        Examples:
        | category           | country  | language      |
        | Action             | Norway   | norwegian     |
        | Adventure          | Italy    | italian       | => Find examples of games categories
        | Board              | India    | english       |
        | Card               | Canada   | french        |
        | Casino             | Brazil   | portuguese    |

    Scenario Outline: Worldwide featurings of a game

    """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
    As a user, I want to be able to see whether a particular game was featured over a selected date range
    so that I can monitor where and when my game (or a competitor) was featured.
    """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

        When I search for <keyword>
        And I select <game> 
        And I don't select any other filter
        Then I should see the list of countries and the title of the group where <game> was Featured

        Examples:
        | keyword     | game                                      |
        | adventure   |  com.kongregate.mobile.adventurecapitalist|
        | gun games   | com.fungames.sniper3d                     |  
        | quest       | com.smallgiantgames.empires               |
        | card        | com.mobilityware.solitaire                |
