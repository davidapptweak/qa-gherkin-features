@REL-472 @Featured-Apps

Feature: Featured Apps on Google (tab Apps)
    Background: I am in Featured Apps page
        Given I have selected Google Play in the filters
          And I am on the tab "Apps"

    Scenario Outline: Most Featured Apps in a category worldwide
    
    """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
    As a user, I want to see which apps were featured the most often in a given country and category on a given date range
    so that I can know which apps are popular on Google Play Store.
    """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
        When I click on <category>
        And I haven't selected any other filters
        Then I should see the most featured apps in <category> in the US, UK, France & Germany

        Examples:
        | category           |
        | Art & Design       |
        | Auto & Vehicules   |
        | Beauty             |
        | Books & Reference  |
        | Business           |

    Scenario Outline: Featured Apps in a category, country and language
        
    """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
    As a user, I want to see which apps were featured in a given country, language and category on a given date 
    so that I can check if any app I know was featured.
    """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

        When I click on <category>
        And I have selected a <country>
        And I have selected a <language>
        Then I should see the apps that were featured in each group in <category> in <country> in <language>

        Examples:
        | category           | country  | language      |
        | Art & Design       | Norway   | norwegian     |
        | Auto & Vehicules   | Italy    | italian       |
        | Beauty             | India    | english       |
        | Books & Reference  | Canada   | french        |
        | Business           | Brazil   | portuguese    |

    Scenario Outline: Worldwide featurings of an app

    """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
    As a user, I want to be able to see whether a particular app was featured over a selected date range
    so that I can monitor where and when my app (or a competitor) was featured.
    """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

        When I search for <keyword>
        And I select <app> 
        And I don't select any other filter
        Then I should see the list of countries and the title of the group where <app> was Featured

        Examples:
        | keyword     | app       |
        | headspace   | 493145008 |
        | waze        | 323229106 |
        | lightroom   | 878783582 |
        | zara        | 547951480 |
        
