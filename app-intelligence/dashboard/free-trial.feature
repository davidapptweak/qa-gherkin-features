@REL-601 @feature @app-intelligence
Feature: Free Trial for App Intelligence

  As a user, I want to be able to test App Intelligence with the data of my choice 
  so that I can really see if the data that I’m interested in is worthwhile 
  and so that I can build relevant sample reports to convince the rest of my team that the subscription is worth the pay. 
  
  Background : App Intelligence Dashboard
    Given I am on "App Intelligence" page

  
  Scenario: New report free trial pop up
    Given I don't have an app intelligence subscription
    When I click on the button to create a new report
    Then I should see the free trial pop up

  Scenario: Free trial activation
    Given I don't have an app intelligence subscription
      And I have clicked on the button to create a new report
    When I click on the button to activate the free trial
    Then I should be redirected to the App Intelligence dashboard
      And I should see a confirmation message
          
  Scenario: Banner to activate App Intelligence subscription immediately
    Given I have an app Intelligence free trial
    When I am on any page of app intelligence
    Then I should see a banner that invites me to subscribe now
    
  Scenario: Button to activate App Intelligence subscription immediately in banner
    Given I am in an app intelligence free trial
    #When I click on the button to 

