@market-intelligence @feature @REL-633
Feature: Benchmark Seasonality
  As a user, I want to compare the seasonality of my app with the seasonality of a category, 
  so that I can see how much my app is affected by seasonality trends. 

  Background: Market Intelligence - Seasonality 
    Given I am in Market Intelligence on the Seasonality page 
      And I have connected my Apple Console or Google Console 

  Scenario: Default is set to no benchmark
    When I go to the Seasonality page 
    Then There is a dropdown to select a benchmark 
      And the default value is set to "No Benchmark"

  Scenario: User selects an app to benchmark 
    When I select an app among my connected apps in the benchmark dropdown
    Then An extra line should be added to the "full seasonality" graph in the "details by category/country" section showing the full seasonality of the benchmarked app
      And a second Y-axis at the scale of the benchmarked app's data should be added to the graph on the right 
      And an extra line should be added to the weekly seasonality graph showing the weekly seasonality of the benchmarked app
      And an extra line should be added to the yearly saesonality graph showing the yearly seasonality of the benchmarked app
