@REL-307 @feature @market-intel
Feature: Revenue Market Share By App
  
  As a user, I want to see the total size of my market in terms of revenue over time 
  so that I can see my market's progression in terms of overall revenue. 
  
  As a user, I want to see a summary of total market size in terms of daily revenue and monthly revenue 
  so that I have a quick knoweldge of my market's size in terms of revenue. 
  
  As a user, I want to see a line or stack graph with the revenue history of the top 10 apps in my market over the last 30 days 
  so that I can immediately spot apps with high growing revenue wihtin my market.
  
  As a user, I want to see the details of these top 10 apps in a table 
  that contains the app's name, the app's share of revenue, the app's average daily revenue and the app's revenue growth over an equivalent time period 
  so that I have some relevant KPIs to benchmark the top revenue apps of my market. 
  
  As a user, I want these apps to be ordered by decreasing average daily revenue in the table, 
  so that the first app I see is the app with the highest average daily revenue within my market. 
  
  As a user, I want to be able to see more than the top 10 revenue apps and go up to the top 50 apps in the table 
  so that I can have a broader view of the top revenue apps within my market. 
  
  As a user, I want to be able to compare the top 10 revenue apps 
  within the same category across countries in a quick view, a line chart, a stack chart or a table, 
  so that I can compare top players from the same category across countries. 
  
  As a user, I want to be able to compare the top 10 revenue apps 
  within the same country across categories in a quick view, a line chart, a stack chart or a table, 
  so that I can compare top players from the same country across categories. 


  Background: Market Intelligence - Market Share By App - Revenue
    Given I am in one of my Market Intelligence markets
      And I open "Market Share By App" page
    
  @simple
  Scenario: Switch from download to revenue 
    When I switch from downloads to revenue
    Then I should see the total market size history graph in terms of revenue instead of downloads
      And I should see the Total Market Size KPIs in terms of revenue instead of downloads
      And I should see the revenue history of the top 10 revenue apps of the market in the line graph 
      And I should see the data of the top apps in market in terms of revenue instead of downloads
      And I should see the data in the Details by Country and category in terms of revenue instead of downloads


  Scenario: Show real data in graph
    When I connect my Apple or Google Console 
    Then I should see the data of any of my apps flagged as real data in all graphs
      And I should see the data of any of my apps flagged as real data in all tables
      
  # for the following scenarios, there are several possibilities. 
  # I did a difference between if the app is in the top 10 apps of the overall market or if the app isn't in that top 10. 
  # However, in the details by country and category the app can be or not be in the top 10. 
  # In the quick view, we only highlight the benchmarked app if it is among the top 10 revenue apps for the selected category and country, if not, nothing is shown. 
  # In the graphs and table, the benchmarked app's data is always added (either it is already in the graph, or it is added as an extra serie) 


  Scenario: Benchmark app revenue in market share by app - app in top 10
    """
    As a user, I want to be able to benchmark my app's revenue with the market 
    so that I can see the market share of my own app
    """

    Given I have connected my Apple or Google Console
      And my app is among the top 10 apps in revenue of my market
    When I select one of my apps to be benchmarked against the market
    Then my app should be highlighted within the "Top apps in market table" table 
      And my app should be highlighted in any "Quick view" of the "Detail by category and country" section where it also appears among the top 10 
      And my app's revenue history should be added to all graphs in the "Detail by category and country" section


  Scenario: Benchmark app revenue in market share by app - app not in top 10
    """
    As a user, I want to be able to benchmark my app's revenue with the market 
    so that I can see the market share of my own app
    """
    
    Given I have connected my Apple or Google Console
      And my app is not among the top 10 apps in revenue of my market
    When I select one of my apps to be benchmarked against the market
    Then my app's revenue history should be added to the "downloads history of top apps in market" as an extra serie 
      And my app should be highlighted within the "Top apps in market table" table as an extra line separated from the others
      And my app should be highlighted in any "Quick view" of the "Detail by category and country" section where it also appears among the top 10
      And my app's revenue history should be added to all graphs in the "Detail by category and country" section


  #TODO @davida
  #some more general tests that apply to this section but are also common to other market intelligence pages include:
  #in every graph, I should have the possibility to download CSV and download image
  #in every graph where there is more than one serie, I should be able to unselect all series (and reselect all) 
  #in every graph, I should be able to see the data on a daily and weekly basis 
  #in "details by category and country", when I'm on the view by category, the data is shown for one country and the categories are compared. I can change the country using the radio buttons
  #in "details by category and country", when I'm on the view by country, the data is shown for one category and the countries are compared. I can change the category using the radio buttons
  #I can view the "downloads history of top apps in market" as a line chart or a stack chart 
  #I can view the "details by category and country" as quick view, line chart, stack chart or table
