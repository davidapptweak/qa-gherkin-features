@feature @REL-625 @aso-intelligence
Feature: New date picker

  # The design of the date pickers should be consistent across the tool (currently one red and one blue).
  # When I click on the date picker the calendar opens to select start & end date 
  # (both dates in one “box”, not start date on one side and end date on the other side) : https://www.loom.com/share/ef948ac1e54243feb558948d6f0ff61e
  # The date picker should always select start date then end date : https://www.loom.com/share/674d7097c21b44688cc1ce73b5984baa

  # I’m able to select only one day of data : https://www.loom.com/share/cef4557efafe4e708d689d5f43e09e1c
  # As a user, I want to be able to select a single date,
  # so that I can see data for that date only (ex: featured apps).


  Background: I am in ASO intelligence page
    Given I am in ASO intelligence page


  Scenario: Show shortcuts
    """
    As a user, I want to have access to quick filters (same as current)
    so that I can rapidly change the date range without having to select them manually.
    """

    When I click on the date picker
    Then I should see 7 shortcuts "last 7 days, last 30 days, last 90 days, This Month, Last Month, Past 6 Months, Past Year, Custom Range"


  Scenario Outline: Select shortcuts
    """
    As a user, I want to have access to quick filters (same as current)
    so that I can rapidly change the date range without having to select them manually.
    """

    When I click on <shortcut>
    Then the date picker should be updated with the <shortcut-value>
      # the data related to the date picker should be updated

      Examples:
      | shortcut      | shortcut-value |
      | Last 7 days   | 7              |
      | Last 30 days  | 30             |
      | Last 90 days  | 90             |
      | This Month    | 30             |
      | Last Month    | 30             |
      | Past 6 Months | 182            |
      | Past Year     | 365            |


  Scenario: Select custom range shortcut
    """
    As a user, I want to select the start then the end date. If one of them was wrong I’ll restart with start then end date
    so that I don’t have to leave the date picker.
    """

    When I click on the date picker
      And I select the start date
      And I select the end date
    But I change the start date
      And I change the end date
      And I apply the selection
    Then I should see the last date range selection in the date picker box


  Scenario: Start date must be prior to the end date
    """

    As a user, I want to select the start then the end date. If one of them was wrong I’ll restart with start then end date
    so that I don’t have to leave the date picker.
    """

    When I click on the date picker
      And I select the start date
      And I select the end date that is prior to the start date
    Then the last date selected should become the start date
      And I should select a new end date


  Scenario: Manually edit the text
    """
    I’m able to edit the text : https://www.loom.com/share/e96957419ff94ed5aee521c25f56ca18

    As a user, I want to be able to change the date by editing the date in the text box,
    so that I don’t have to navigate through the calendar if I want to go back 2 years.
    """

    When I click on the date picker
    Then the text cursor (I) should appear in the field
      And it should allow me to edit the date manually
      And the calendar should update
    #TODO the key "enter" works as the "apply" button


  Scenario: Apply date selection with button
    """
    The data should load once the start AND end date selection has been done (add apply button)
    As a user, I want the data to load once I have finished indicating my full date selection (start and end)
    so that I don’t have to wait for all the data to load every time I change the start date and before being able to select the end date.
    """

    Given I have selected a custom date range
    When I click on the button to apply
    Then I should see the date picker close and start loading the data
    # Don't load the data as long as start and end date have been chosen and "apply" has been selected


  Scenario: Apply date selection with key Enter
    Given I have selected a custom date range
    When I click on Enter on my keyboard
    Then I should see the date picker close and start loading the data
