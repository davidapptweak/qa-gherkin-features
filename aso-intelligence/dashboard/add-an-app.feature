@aso-intel @feature @REL-317
Feature: Add an App widget on ASO Page

  As a user, I want to be able to add more than one competitor in the Add an app widget, 
  so that I can compare my app’s performance with a set of competitors. 

  As a user, I want to be able to add the competitors I want to follow myself, or select among suggestions, 
  so that I have a source of inspiration if I don’t know where to start. 

  As a user, I want to be able to choose to follow competitors in all followed countries or in one particular country, 
  so that I can add major competitors to all localisations at once while being able to tailor according to each market. 

  As a user, I want to be able to add more than one keyword in the Add an app widget, 
  so that I can already check my app’s performance on a set of keywords. 

  As a user, I want to be able to add the keywords I want to follow myself, or select among suggestions, 
  so that I have a source of inspiration if I don’t know where to start. 

  As a user, I want to be able to choose to follow keywords in all followed countries or in one particular country, 
  so that I can add brand keywords to all localisations at once while being able to tailor each keyword list according to every market. 


  Background: On ASO Page
    Given I am on "ASO Dashboard" page
      And I click on "Add an app"
  
  
  @simple @focus
  Scenario: Simple add an app
    When I select as device : "Iphone"
      And I add an app : "Candy Crush Saga"
      And I choose as country : "Japan"
      And I go to confirm tab to see an summary of my selection
    Then I add them to my dashboard
      And my new app should be added to my followed apps list
    

  Scenario: Add an app in various countries
    """"
      As a user, I want to be able to immediately follow an app in various countries, 
      so that I don’t have to add my app in all its available countries one by one.
    """

    When I add an app : "Candy Crush Saga"
      And I choose as country : "Canada" in "French (Canada)"
      And I choose as country : "France"
      And I choose another country : "Japan"  in "Japanese"
      And I go to confirm tab to see an sumary of my selection
      And I add to my dashboard
    Then my new app should be added to my followed apps list
