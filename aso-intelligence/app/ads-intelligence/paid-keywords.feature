@REL-878 @feature @aso-intelligence  @ads-intelligence
Feature: Paid keywords

    As a user, I want a cross competition view on each of my paid keywords from the last 90 days to see which ones are the most coveted by my competitors
    so that I can adapt by bidding strategy.


   Background: Paid keywords
      Given I am on ASO Intelligence page
       And I have selected an Android App
       And I am on the Ads Intelligence tab

   Scenario: I selected my app and I see where it is featured and when
      When I click on the tab "Paid Keywords"
      Then I should see a table with all the paid keywords for this app in the last 90 days
      And I should in the table, per keyword, the rank of my app
      And I should in the table, per keyword, the volume
      And I should in the table, per keyword, the difficulty for the app
      And I should in the table, per keyword, the ad frequency for the app
      And I should in the table, per keyword, the ad frequency for the other apps that appeared on this keyword

   Scenario: I selected my app and I see where it is featured and when
      Given I selected an app with paid keywords on the playstore
      When I click on one paid keywords from the list of paid keywords
      Then I should see a modale with the list of apps that bid on this keywords with their count percentage
      And I should also see in the modale the list of the Top 10 Live Search Results for this keyword

