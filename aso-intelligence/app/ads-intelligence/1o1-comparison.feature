@REL-650 @feature @aso-intelligence @ads-intelligence
Feature: Ads Intelligence - 1o1 Comparison

    As a user, I want to see the data on google ads from the play store
    so that I can understand on which keywords my competitors are bidding on.
    and so that I can see which keywords I share with my competitors and which ones are different.


  Background: Ads Intelligence
      Given I am on "ASO Dashboard" page
        And I have selected an Android App
        And I click on "Ads Intelligence" tab
        And I open the 1o1 comparison tab

  Scenario: See a pop-up asking me to add competitors
      Given I click on "Ads Intelligence" tab
      When I click on "1o1 comparison"
      Then I should see a pop "Add a competitor"

  Scenario: See the unique/shared terms repartition
      Given I added at least one competitor
      When I select two apps of my choice
      Then I should see two circles

   Scenario: See the unique/shared terms repartition
      Given I added the competitor "app name"
      When I select the competitor "app name" on one of the drop-downs
      And I select my app "app name" on the other drop-down
      Then I should see two circles overlapping

    Scenario: See the top shared keyword terms
      Given I added the competitor "app name"
      When I select the competitor "app name" on one of the drop-downs
      And I select my app "app name" on the other drop-down
      Then I should see the top shared keywords updated

    @simple @smoke
    Scenario: See the list of unique keywords
      Given I added the competitor "app name"
      When I select the app "app name" on the left drop-down
      Then I should see the list of unique keywords

   Scenario: See the list of shared keywords
      Given I added the competitor "app name"
      When I select the app "app name" on one of the drop-downs
      And I select the app "app name" on the other drop-down
      Then I should see the list of shared keywords

   Scenario: See the list of unique keywords
      Given I added the competitor "app name"
      When I select the app "app name" on the right drop-down
      Then I should see the list of unique keywords

