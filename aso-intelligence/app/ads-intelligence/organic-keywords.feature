@REL-877 @feature @aso-intelligence @ads-intelligence
Feature: Ads Intelligence - Organic Keywords

  As a user I want to see if my competitors are appearing more than my app on keywords that bring me the most organic downloads
  So that I can adapt my bidding strategy


  Background: Ads Intelligence
    Given I am on ASO Intelligence page
      And I have selected an Android app
      And I am on Ads Intelligence page

  Scenario: Page organic downloads
    When I click on "organic downloads" tab
    Then I should see a semi-cirle graph representing the Ad frequency coverage
      And I should see a bar chart representing the Competition ad frequency coverage
      And I should see a semi-circle graph representing the Ads Protection Status of my app
      And I should see a table with the top 50 keywords that brings my app the most downloads
      And I should see for each keyword: Its download estimation, Rank, Volume, Difficulty, Protection, count percentage, all apps count percentage for this keyword

  Scenario: Page organic downloads
    When I click on a keyword in the table of organic downloads keywords
    Then I should see a pop-up with the top apps that bid on this keywords ranked by count percentage
      And I should see in the pop-up the top 10 Live Search Results for this keyword
      And I should see in the pop-up the volume, the number of results and difficulty for this keyword
