@REL-876 @feature @aso-intelligence @ads-intelligence
Feature: Ads Intelligence - Cross competition

  As a user, I want to see the cross competition overview of all the bidding keywords
  so that I can understand on which keywords my competitors are bidding on.
  and so that I can see which keywords I share with my competitors and which ones are different.
  and so that I can quickly identify which competitors are bidding on specific keywords

  Background: Ads Intelligence
    Given I selected an Android App
      And I clicked on ASO Intelligence page
      And I clicked on Ads Intelligence page
      And I clicked on the cross competition tab


  Scenario: See a pop-up asking me to add competitors
    When I click on "Cross competition" tab
    Then I should see a pop-up "Add a competitor"

  Scenario: See the whole list of bidding keywords my app and my competitors are bidding on with their count
    Given I added at least one competitor
    When  I click on "all keywords terms" tab
    Then I should see the list of all the bidding keywords of my app
      And I should see the list of all the bidding keywords of the competitor
      And I should see for each bidding keyword a count of all the combinations containing that keyword for my app
      And I should see for each bidding keyword a count of all the combinations containing that keyword for my competitor

  Scenario: See the list of combinations for a bidding keyword
    Given I was on "all keywords terms" tab
      And I added a competitor
      And I had a list of keywords my app is bidding on
    When I click on the count of combinations for a bidding keyword of my app
    Then I should see a pop up with the list of all the combinations for that bidding keyword for my app

  Scenario: See the whole list of bidding keywords my app shares with at least one competitor
    Given I added at least one competitor
    When I clik on "Shared keyword terms" tab
    Then I should see the list of all the bidding keywords my app shares with at least one competitor
      And I should see for each bidding keyword a count of all the combinations containing that keyword for my app
      And I should see for each bidding keyword a count of all the combinations containing that keyword for my competitors

  Scenario: See the list of combinations for a shared bidding keyword
    Given I was on "Shared keyword terms" tab
      And I added at least one competitor I share bidding keywords with
    When  I click on the count of combinations for a keyword
    Then I should see a pop up with the list of all the combinations for that bidding keyword of my app

  Scenario: See the whole list of unique bidding keywords for my app and my competitors
    Given I added at least one competitor
    When I clik on "Unique Keyword terms" tab
    Then I should see the list of all the unique bidding keywords of my app
      And I should see the list of all the unique bidding keywords of my competitor
      And I should see for each bidding keyword a count of all the combinations containing that keyword for my app
      And I should see for each bidding keyword a count of all the combinations containing that keyword for my competitors

  Scenario: See the list of combinations for a unique bidding keyword
    Given I added at least one competitor
      And I was on "Unique keyword terms" tab
    When I click on the count of combinations for a keyword
    Then I should see a pop up with the list of all the combinations for that bidding keyword
