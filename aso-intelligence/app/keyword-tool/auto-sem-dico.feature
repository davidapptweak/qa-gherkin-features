@REL-915 @feature @auto-sem-dico

Feature: Automatic Semantic Dictionary

  Background: I am in the Keyword Tool 
    Given I am on "ASO Dasboard" page
      And I have selected an app
      And I have clicked on "Keywords" tab


  @simple
  Scenario: User clicks on "Create Brainstorm list" button 
    When I click on the "Create Brainstorm list" button 
    Then I should be shown a pop up that explains what this button does


  Scenario: User launches the Brainstorm list
    Given I am seeing the pop up that explains what the "Create Brainstorm list" does 
    When I click "Start the Brainstorm" 
    Then the "create brainstorm list" button should change color
      And it should say "creating brainstorm list" 
      And it should contain a small loader


  Scenario: Brainstorm list is added to keyword lists in grey
    Given I have launched the Brainstorm list 
      And the brainstorm list is still building 
    When I click on the keyword list dropdown
    Then I should see a new list named "Brainstorm" in my keyword lists
      And that list shouwed be greyed (inaccessible) 


  Scenario: Brainstorm list is ready 
    Given I have launched the Brainstorm list 
    When the Brainstorm list is ready 
    Then I little notification should appear on the keyword list dropdown 
      And there should be a list called "Brainstorm" among my keyword lists
      And the "Create Brainstorm button" should return to its initial state


  Scenario: Access the Brainstorm list 
    Given I have launched the Brainstorm List 
      And the Brainstorm list is ready 
    When I click on the "Brainstorm" list in my keyword lists 
    Then I should be shown the Brainstorm list in the keyword tool
      
