@REL-938 @feature @aso-intelligence
Feature: Explore-featured

  As a user, I want to see for my app in which category I am featured during a certain date range on the Google Play
  so that I see what is my presence in these categories.

  As a user, I want to see for each day the app that was featured, the category in which it was featured
  so that I can see how regularly my app is featured and in which category

  Background: Be on selecte app Explore tab
    Given "I am on "ASO Dashboard" page
      And I have selected an Android App
      # And I am on the tab "Explore"

  Scenario: See the featured of the selected app
    When I click on the tab "Explore"
      And I selected a date range
    Then I should see the categories in which my app is feature within the selected date range
      And I should see for each day, in which category my app has been feature

