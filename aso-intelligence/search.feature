@aso-intel @feature @REL @REL-316
Feature: New Search app on ASO Intelligence page


  Background: On ASO Intelligence Page
    Given I am on "ASO Intelligence" page


  @simple @smoke @focus
  Scenario: Simple search
    When I search for an app : "Candy Crush Saga"
    Then I should see suggestions in dropdown
      And I press on the search button
      And I'm sent to the results page


  @focus
  Scenario Outline: Live Search with dropdown suggestions
    """
    As a user, I want to see the results for my search as I type my app name,
    so that I can rapidly see if AppTweak finds my app.
    """

    When I search for an app : <term>
    Then I should see <app-name> on <app-device> in the dropdown
      And I should see the dropdown filled with some results

    Examples:
      | term   | app-name              | app-device |
      | duol   | Duolingo              | android    |
      | candy  | Candy Crush Soda Saga | android    |
      | face   | Facebook              | android    |
      | netf   | Netflix               | iphone     |
      | fortni | Fortnite              | iphone     |


  @focus
  Scenario Outline: Search and go to the matching app overview
    """
    As a user, I want to be able to access the Android, iPhone version of an app just as quickly,
    so that I don’t have to do a longer process if my preference don’t match AppTweak default
    """
    
    When I search for an app : <term>
    And I choose <app-name> on <app-device> in the dropdown
    Then I should be redirected to the chosen app "Overview Page" with matching id "<app-id>"

    Examples:
      | term    | app-name              | app-device | app-id                      |
      | duol    | Duolingo              | android    | com.duolingo                |
      | candy   | Candy Crush Soda Saga | android    | com.king.candycrushsodasaga |
      | face    | Facebook              | android    | com.facebook.katana         |
      | surfers | Subway Surfers        | iphone     | 512939461                   |
      | netf    | Netflix               | iphone     | 363590051                   |
      | fortni  | Fortnite              | iphone     | 1261357853                  |
      
  Scenario Outline: Suggested results for Google-Only countries
  """ 
  As a product owner, if users select a country that we only support on Google, I want users to only see results in the Play Store in the quick results, so that they don't see empty results for the App Store.
  """
  
  When I select a google only country: <google-only-country>
    And I search for an app: <term>
  Then I should only see Google apps in the suggested results "<app-device>"
  Examples:
    | google-only-country| term | app-device |
    | Bangladesh | Facebook | android |
    | Bangladesh | Netflix | android |
    | Bolivia | Facebook | android |
    | Bolivia | Netflix | android |
    | Laos | Facebook | android |
    | Laos | Netflix | android |

  Scenario: Result page for google only countries
  """ 
  As a product owner, if users select a country that we only support on Google, I want users to see results in the Play Store column of the results page, and to see a message in the iPhone and iPad columns of the results page so that they understand that there are no results for the App Store in these countries.
  """
  When I select a google only country
    And I search for an app
    And I press on the search button to access the results page 
  Then I should see the results of my search in the Google Play column
    And I should see a message in the iPad and iPhone columns that informs me that the App Store doesn't support this country

  ## TODO @davida 
  # As a user, I want to be able to rapidly access an app without going through the results page
  # if the app appears among the suggestions,
  # so that I access my app in just a click.
  
  ## TODO @davida 
  # As a user, I want to access the search results page in case I don’t see my app in the suggestions,
  # so that I have access to more results.

  ## TODO @davida 
  # As a user, I want AppTweak to show the countries I usually work with first in the country drop down,
  # so that I don’t have to search through the drop down.